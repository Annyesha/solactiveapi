from datetime import datetime
import flask
from DBAccess import DBAccess
from flask import Flask, request, jsonify

app = Flask(__name__)


@app.route('/products/newproduct/', methods=['GET', 'POST'])
@app.route('/products/newproduct', methods=['GET', 'POST'])
def addNewProduct():
    # Executed for Post request
    if request.method == 'POST':
        try:
            jsonRequest = flask.request.json
            # Creates the Product in database and gets the id
            productId = DBAccess.addNewProduct(jsonRequest['asOf'], jsonRequest['product_name'], jsonRequest['business'],
                                               jsonRequest['currency'], jsonRequest['serviceAttributes'])

            return jsonify("The product is created with Id :" + str(productId)), 200
        except:
            return jsonify("Error in creating the New Product"), 400
    else:
        return jsonify('This is not a get method'), 400


@app.route('/products/<int:prod_id>/')
@app.route('/products/<int:prod_id>')
def showProduct(prod_id):
    try:
        # Reads the timestamp from request parameter
        timestamp = datetime.fromtimestamp(int(request.args['timestamp']))
        if timestamp is None:
            timestamp = datetime.now()

        # Get the product from the database
        productJson = DBAccess.showProduct(prod_id, timestamp)
        if productJson is not None and len(productJson) > 0:
            return productJson, 200
        else:
            return jsonify("Product not found within specified timestamp"), 200
    except:
        return jsonify("Error occured while finding the Product :" + str(prod_id)), 400


@app.route('/services/<int:service_id>/')
def getProductsByService(service_id):
    try:
        timestamp = datetime.fromtimestamp(int(request.args['timestamp']))
        if timestamp is None:
            timestamp = datetime.now()

        # Get all the valid products from database for the provided Service
        productsJson = DBAccess.getProductsByService(service_id, timestamp)
        if productsJson is not None and len(productsJson) > 0:
            return jsonify(productsJson), 200
        else:
            return jsonify("No Products found for the Service : " + str(service_id)), 200
    except:
        return jsonify("Error occured while getting Products for the Service : " + str(service_id)), 400


@app.route('/services/<int:service_id>/product/<int:prod_id>/', methods=['GET', 'PUT'])
def editServiceAttributesOfProduct(service_id, prod_id):
    jsonRequest = flask.request.json
    if request.method == 'PUT':
        try:
            # Update the attribute value of the service for the requested product
            returnCode = DBAccess.editServiceAttributesOfProduct(service_id, prod_id, jsonRequest['attributeName'],
                                                                 jsonRequest['attributeValue'])

            if returnCode is not None and returnCode == 1:
                return jsonify('Service Attributes updated for ' + str(prod_id)), 200
            else:
                return jsonify('Service Attributes cannot be updated for ' + str(prod_id)), 200
        except:
            return jsonify('Service Attributes updated ERROR for ' + str(prod_id)), 400
    else:
        return jsonify('Get is not supported')


app.debug = True
app.secret_key = 'secret_key'
app.run(port=6000)
