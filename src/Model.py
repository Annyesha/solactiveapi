from sqlalchemy import create_engine, Column, ForeignKey, Integer, String, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

Base = declarative_base()


class Product(Base):
    __tablename__ = 't_product'

    id = Column(Integer, primary_key=True)
    name = Column(String(250), nullable=False, unique=True)
    business = Column(String(250))
    currency = Column(String(3))
    asOf = Column(DateTime)
    createdDate = Column(DateTime)

    @property
    def serialized(self):
        return {
            'id': self.id,
            'name': self.name,
            'business': self.business,
            'currency': self.currency,
            'asOf': self.asOf,
            'createdDate': self.createdDate
        }


class Service(Base):
    __tablename__ = 't_service'

    id = Column(Integer, primary_key=True)
    name = Column(String(250), nullable=False, unique=True)
    asOf = Column(DateTime)

    @property
    def serialized(self):
        return {
            'id': self.id,
            'name': self.name
        }


class ProductServiceAttributes(Base):
    __tablename__ = 't_products_serviceattr'
    id = Column(Integer, primary_key=True)
    productId = Column(Integer, ForeignKey('t_product.id'))
    product = relationship(Product)
    serviceId = Column(Integer, ForeignKey('t_service.id'))
    service = relationship(Service)
    attributeName = Column(String(250), nullable=False)
    attributeValue = Column(String(250), nullable=False)
    asOf = Column(DateTime)

    @property
    def serialized(self):
        return {
            'product': self.productId,
            'service': self.serviceId,
            'attributeName': self.attributeName,
            'attributeValue': self.attributeValue
        }


engine = create_engine('sqlite:///solactive.db')
Base.metadata.create_all(engine)
