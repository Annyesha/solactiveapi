from datetime import datetime, timedelta
from sqlalchemy import create_engine, and_, inspect
from sqlalchemy.orm import sessionmaker
from Model import Base, Product, Service, ProductServiceAttributes

engine = create_engine('sqlite:///solactive.db', connect_args={'check_same_thread': False})
Base.metadata.bind = engine
DBSession = sessionmaker(bind=engine)


# Class with static methods for all the database operations
class DBAccess:
    @staticmethod
    def addNewProduct(asOf, name, business, currency, serviceAttributes):
        session = DBSession()
        currentDateTime = datetime.now()
        validTillDate = datetime.fromtimestamp(int(asOf))
        try:
            # Create the product with the provided attribute values
            newProduct = Product(name=name, business=business,
                                 currency=currency, asOf=validTillDate, createdDate=currentDateTime)
            session.add(newProduct)
            # create the new services if new services found in attribute list
            DBAccess.createNewServices(serviceAttributes, session)
            # Read the service attributes from the list and map them respective product
            for attributeName in serviceAttributes:
                previousAttributeName = None
                previousServiceId = None
                # Get the Service Name from the attribute name
                serviceName = attributeName.split('.')[0]
                # No need to check for Service Id in database if the service of this attribute and
                # service of the previous attribute are same
                if previousAttributeName is not serviceName:
                    # Get the Service Id from Service table for the service attribute
                    service = session.query(Service).filter(Service.name == serviceName).first()
                    previousServiceId = service.id
                    previousAttributeName = attributeName

                newProdServiceAttribute = ProductServiceAttributes(productId=newProduct.id,
                                                                   serviceId=previousServiceId,
                                                                   attributeName=attributeName,
                                                                   attributeValue=serviceAttributes[attributeName])
                session.add(newProdServiceAttribute)

            # Session is committed for product, service and product service attributes
            session.commit()
            return newProduct.id
        except:
            session.rollback()
            raise
        finally:
            session.close()

    @staticmethod
    def createNewServices(attributeList, session):
        serviceNamelist = []
        # Read and store the unique service names from attribute names
        for attributeName in attributeList:
            if attributeName not in serviceNamelist:
                serviceNamelist.append(attributeName.split('.')[0])
        # Create new services for all the unique service names found
        # only after checking they do not exist
        for serviceName in serviceNamelist:
            service = session.query(Service).filter(Service.name == serviceName).first()
            if service is None:
                # AsOf date of the services are set to current date
                # This can be read from request, scope of improvement
                currentDateTime = datetime.now()
                newService = Service(name=serviceName, asOf=currentDateTime)
                session.add(newService)
        return

    @staticmethod
    def showProduct(prod_id, timestamp):
        session = DBSession()
        try:
            # Get the product if timestamp is greater than asOf
            product = session.query(Product).filter(and_(Product.id == prod_id, timestamp >= Product.asOf)).first()
            if product is not None:
                # Get the corresponding service attributes of the product only when the timestamp is greater than
                # the asOf value of the respective services
                serviceAttributes = session.query(ProductServiceAttributes, Service).filter(
                    ProductServiceAttributes.productId == product.id).filter(
                    and_(timestamp >= Service.asOf, Service.id == ProductServiceAttributes.serviceId)).all()
                # Formulate the json for the product
                productJson = {c: getattr(product, c) for c in inspect(product).attrs.keys()}
                # Formulate the json for the service attributes
                serviceAttributeJson = {}
                for serviceAttribute, service in serviceAttributes:
                    serviceAttributeJson[serviceAttribute.attributeName] = serviceAttribute.attributeValue
                # Append the service attribute to the product
                productJson['serviceAttributes'] = serviceAttributeJson
                return productJson
            else:
                return None

        except:
            raise
        finally:
            session.close()

    @staticmethod
    def getProductsByService(service_id, timestamp):
        session = DBSession()
        try:
            # Get the 7 days old time
            sevenDaysOldDateTime = datetime.now() - timedelta(days=7)
            listOfProdAndAttributes = []
            # Get all the products along with its attributes related to the Service Id
            # Validity of the product and service is checked against the timestamp
            for p, ps, s in session.query(Product, ProductServiceAttributes, Service).filter(and_(
                    Product.createdDate >= sevenDaysOldDateTime, timestamp >= Product.asOf)).filter(
                and_(timestamp >= Service.asOf, Service.id == ProductServiceAttributes.serviceId)).filter(
                and_(ProductServiceAttributes.serviceId == service_id,
                     ProductServiceAttributes.productId == Product.id)).all():
                isEntered = 0
                # Formulate the Json for the products and related service attributes
                # If the temp list already contains one entry
                if len(listOfProdAndAttributes) > 0:
                    # Check for all the products in the temp list and add the service attribute of
                    # the current product to the product node in the list, if the product already
                    # exists in the temp list
                    for dic in listOfProdAndAttributes:
                        if ('productName', p.name) in dic.items():
                            serviceAttribute = {ps.attributeName: ps.attributeValue}
                            dic['serviceAttributes'].append(serviceAttribute)
                            isEntered = 1
                            break
                    # The product and the corresponding service added to the list when the product
                    # does not already exist in temp list
                    if isEntered == 0:
                        services = []
                        service = {ps.attributeName: ps.attributeValue}
                        services.append(service)
                        productKey = {'serviceAttributes': services, 'productName': p.name}
                        listOfProdAndAttributes.append(productKey)
                # The first entry of the product and the corresponding service attribute
                # executed only once, for the first record only
                else:
                    services = []
                    service = {ps.attributeName: ps.attributeValue}
                    services.append(service)
                    productKey = {'serviceAttributes': services, 'productName': p.name}
                    listOfProdAndAttributes.append(productKey)

            return listOfProdAndAttributes
        except:
            raise
        finally:
            session.close()

    @staticmethod
    def editServiceAttributesOfProduct(service_id, prod_id, attributeName, attributeValue):
        session = DBSession()
        try:
            # Get the service attributes of the product for the requested service
            serviceAttributes = session.query(ProductServiceAttributes).filter(and_(
                ProductServiceAttributes.serviceId == service_id, ProductServiceAttributes.productId == prod_id)).all()
            # Identify the requested attribute name from the fetched attribute list and update the value
            if serviceAttributes is not None:
                for attribute in serviceAttributes:
                    if attribute.attributeName == attributeName:
                        attribute.attributeValue = attributeValue
                        session.commit()
                        return 1
            else:
                return 0
        except:
            raise
        finally:
            session.close()
